use crate::index_file::types::*;
use crate::search_strings::SearchExpr;
use crate::{common::parser::*, search_strings::parse_search_expr};

use nom::{
    branch::alt, bytes::complete::tag, character::complete::space1, combinator,
    multi::separated_list1, sequence::tuple, IResult,
};

fn parse_line(s: &str) -> IResult<&str, Line> {
    combinator::map(
        tuple((
            parse_search_expr,
            space1,
            separated_list1(space1, parse_change),
        )),
        |x| Line {
            search_expr: x.0,
            changes: x.2,
        },
    )(s)
}

#[test]
fn test_parse_line() {
    assert!(test_eq(
        parse_line("tag:a and tag:b +c").unwrap().1,
        Line {
            search_expr: SearchExpr::And(
                Box::new(SearchExpr::Tag("a".parse().unwrap())),
                Box::new(SearchExpr::Tag("b".parse().unwrap()))
            ),
            changes: [Change::AddTag("c".to_string())].to_vec(),
        }
    ));
    assert!(test_eq(
        parse_line("tag:a +c -d correspondent:e").unwrap().1,
        Line {
            search_expr: SearchExpr::Tag("a".parse().unwrap()),
            changes: [
                Change::AddTag("c".to_string()),
                Change::RemoveTag("d".to_string()),
                Change::SetCorrespondent("e".to_string())
            ]
            .to_vec(),
        }
    ))
}

fn parse_add_tag(s: &str) -> IResult<&str, Change> {
    combinator::map(tuple((tag("+"), parse_text)), |x| Change::AddTag(x.1))(s)
}

fn parse_remove_tag(s: &str) -> IResult<&str, Change> {
    combinator::map(tuple((tag("-"), parse_text)), |x| Change::RemoveTag(x.1))(s)
}

fn parse_set_correspondent(s: &str) -> IResult<&str, Change> {
    combinator::map(tuple((tag("correspondent:"), parse_text)), |x| {
        Change::SetCorrespondent(x.1)
    })(s)
}

fn parse_change(s: &str) -> IResult<&str, Change> {
    alt((parse_add_tag, parse_remove_tag, parse_set_correspondent))(s)
}
