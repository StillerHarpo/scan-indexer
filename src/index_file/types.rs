use crate::search_strings;
use crate::search_strings::types::SearchExpr;

#[derive(Clone, Debug, PartialEq)]
pub enum Change {
    AddTag(String),
    RemoveTag(String),
    SetCorrespondent(String),
}

#[derive(Clone, Debug)]
pub struct Line {
    pub search_expr: SearchExpr,
    pub changes: Vec<Change>,
}

#[cfg(test)]
pub fn test_eq(l1: Line, l2: Line) -> bool {
    search_strings::types::test_eq(l1.search_expr, l2.search_expr) && l1.changes == l2.changes
}
