use crate::common::parser::*;
use crate::search_strings::types::*;
use chrono::prelude::*;
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{anychar, space1},
    combinator,
    error::make_error,
    multi::{count, many1},
    sequence::tuple,
    IResult,
};
use regex::Regex;

fn parse_regex(s: &str) -> IResult<&str, Regex> {
    let (rest, regexstr) = parse_text(s)?;
    regexstr
        .parse()
        .map_err(|_| nom::Err::Failure(make_error(s, nom::error::ErrorKind::Fail)))
        .map(|regex| (rest, regex))
}

/// parse an atom with tag tag
fn parse_tag(s: &str) -> IResult<&str, SearchExpr> {
    combinator::map(tuple((tag("tag:"), parse_regex)), |x| SearchExpr::Tag(x.1))(s)
}

#[test]
fn test_parse_tag() {
    assert!(test_eq(
        parse_tag("tag:foo").unwrap().1,
        SearchExpr::Tag("foo".parse().unwrap())
    ));
    assert!(test_eq(
        parse_tag("tag:\"foo bar\"").unwrap().1,
        SearchExpr::Tag("foo bar".parse().unwrap())
    ));
}

/// parse an atom with tag correspondent
fn parse_correspondent(s: &str) -> IResult<&str, SearchExpr> {
    combinator::map(tuple((tag("correspondent:"), parse_regex)), |x| {
        SearchExpr::Correspondent(x.1)
    })(s)
}

#[test]
fn test_parse_correspondent() {
    assert!(test_eq(
        parse_correspondent("correspondent:foo").unwrap().1,
        SearchExpr::Correspondent("foo".parse().unwrap())
    ));
    assert!(test_eq(
        parse_correspondent("correspondent:\"foo bar\"").unwrap().1,
        SearchExpr::Correspondent("foo bar".parse().unwrap())
    ));
}

/// parse an atom with tag content
fn parse_content(s: &str) -> IResult<&str, SearchExpr> {
    combinator::map(tuple((tag("content:"), parse_regex)), |x| {
        SearchExpr::Content(x.1)
    })(s)
}

#[test]
fn test_parse_content() {
    assert!(test_eq(
        parse_content("content:foo").unwrap().1,
        SearchExpr::Content("foo".to_string().parse().unwrap())
    ));
    assert!(test_eq(
        parse_content("content:\"foo bar\"").unwrap().1,
        SearchExpr::Content("foo bar".to_string().parse().unwrap())
    ));
}

/// parse a date in nom
fn parse_date(s: &str) -> IResult<&str, NaiveDate> {
    let (rest, datestr) = combinator::recognize(count(anychar, 10))(s)?;
    datestr
        .parse()
        .map_err(|_| nom::Err::Failure(make_error(s, nom::error::ErrorKind::Fail)))
        .map(|date| (rest, date))
}

/// parse date from
fn parse_date_from(s: &str) -> IResult<&str, SearchExpr> {
    combinator::map(parse_date, SearchExpr::DateFrom)(s)
}

#[test]
fn test_parse_date_form() {
    assert!(test_eq(
        parse_date_from("2022-12-12").unwrap().1,
        SearchExpr::DateFrom(NaiveDate::from_ymd_opt(2022, 12, 12).unwrap())
    ))
}

/// parse date to
fn parse_date_to(s: &str) -> IResult<&str, SearchExpr> {
    combinator::map(parse_date, SearchExpr::DateTo)(s)
}

fn parse_expr_parens(s: &str) -> IResult<&str, SearchExpr> {
    combinator::map(tuple((tag("("), parse_search_expr, tag(")"))), |x| x.1)(s)
}
fn parse_not_expr(s: &str) -> IResult<&str, SearchExpr> {
    combinator::map(tuple((tag("not"), many1(tag(" ")), parse_atom_expr)), |x| {
        SearchExpr::Not(Box::new(x.2))
    })(s)
}

fn parse_or_expr(r: Box<SearchExpr>, s: &str) -> IResult<&str, SearchExpr> {
    combinator::map(tuple((space1, tag("or"), space1, parse_atom_expr)), |x| {
        SearchExpr::Or(r.clone(), Box::new(x.3))
    })(s)
}

/// parse an and expression
fn parse_and_expr(r: Box<SearchExpr>, s: &str) -> IResult<&str, SearchExpr> {
    combinator::map(tuple((space1, tag("and"), space1, parse_atom_expr)), |x| {
        SearchExpr::And(r.clone(), Box::new(x.3))
    })(s)
}

fn parse_search_expr_rec(r: SearchExpr, s: &str) -> IResult<&str, SearchExpr> {
    match combinator::flat_map(
        alt((
            |s| parse_or_expr(Box::new(r.clone()), s),
            |s| parse_and_expr(Box::new(r.clone()), s),
        )),
        |r1| move |s| parse_search_expr_rec(r1.clone(), s),
    )(s)
    {
        Err(_) => Ok((s, r.clone())),
        r2 => r2,
    }
}

fn parse_atom_expr(s: &str) -> IResult<&str, SearchExpr> {
    alt((
        parse_expr_parens,
        parse_content,
        parse_tag,
        parse_correspondent,
        parse_not_expr,
        parse_date_from,
        parse_date_to,
    ))(s)
}

/// parse an expression
pub fn parse_search_expr(s: &str) -> IResult<&str, SearchExpr> {
    let (s, r) = parse_atom_expr(s)?;
    parse_search_expr_rec(r.clone(), s)
}

/// parse a search string
pub fn parse_search_string(s: &str) -> Result<SearchExpr, String> {
    parse_search_expr(s).map(|x| x.1).map_err(|x| x.to_string())
}

#[test]
fn test_parse_search_expr() {
    assert!(test_eq(
        parse_search_expr("tag:a").unwrap().1,
        SearchExpr::Tag("a".parse().unwrap())
    ));
    let a_or_b = SearchExpr::Or(
        Box::new(SearchExpr::Tag("a".parse().unwrap())),
        Box::new(SearchExpr::Tag("b".parse().unwrap())),
    );
    assert!(test_eq(
        parse_search_expr("tag:a or tag:b").unwrap().1,
        a_or_b.clone()
    ));
    let c_and_d = SearchExpr::And(
        Box::new(SearchExpr::Tag("c".parse().unwrap())),
        Box::new(SearchExpr::Tag("d".parse().unwrap())),
    );
    assert!(test_eq(
        parse_search_expr("tag:c and tag:d").unwrap().1,
        c_and_d.clone()
    ));
    let not_e: SearchExpr = SearchExpr::Not(Box::new(SearchExpr::Tag("e".parse().unwrap())));
    assert!(test_eq(
        parse_search_expr("not tag:e").unwrap().1,
        not_e.clone()
    ));
    assert!(test_eq(
        parse_search_expr("tag:a or tag:b or tag:c").unwrap().1,
        SearchExpr::Or(
            Box::new(a_or_b.clone()),
            Box::new(SearchExpr::Tag("c".parse().unwrap()))
        )
    ));
    assert!(test_eq(
        parse_search_expr("(tag:a)").unwrap().1,
        SearchExpr::Tag("a".parse().unwrap()),
    ));
    assert!(test_eq(
        parse_search_expr("(tag:a or tag:b)").unwrap().1,
        a_or_b.clone()
    ));
    assert!(test_eq(
        parse_search_expr("tag:c or (tag:a or tag:b)").unwrap().1,
        SearchExpr::Or(
            Box::new(SearchExpr::Tag("c".parse().unwrap())),
            Box::new(a_or_b.clone())
        )
    ));
    assert!(test_eq(
        parse_search_expr("tag:a or tag:b and not ((tag:c and tag:d) or not tag:e)")
            .unwrap()
            .1,
        SearchExpr::And(
            Box::new(a_or_b),
            Box::new(SearchExpr::Not(Box::new(SearchExpr::Or(
                Box::new(c_and_d.clone()),
                Box::new(not_e.clone())
            ))))
        )
    ))
}
