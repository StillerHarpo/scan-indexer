use chrono::prelude::*;
use regex::Regex;

#[derive(Clone, Debug)]
pub enum SearchExpr {
    Content(Regex),
    Tag(Regex),
    Correspondent(Regex),
    DateFrom(NaiveDate),
    DateTo(NaiveDate),
    And(Box<SearchExpr>, Box<SearchExpr>),
    Or(Box<SearchExpr>, Box<SearchExpr>),
    Not(Box<SearchExpr>),
}

#[cfg(test)]
pub fn test_eq(se1: SearchExpr, se2: SearchExpr) -> bool {
    match (se1, se2) {
        (SearchExpr::Content(r1), SearchExpr::Content(r2)) => r1.to_string() == r2.to_string(),
        (SearchExpr::Tag(r1), SearchExpr::Tag(r2)) => r1.to_string() == r2.to_string(),
        (SearchExpr::Correspondent(r1), SearchExpr::Correspondent(r2)) => {
            r1.to_string() == r2.to_string()
        }
        (SearchExpr::DateFrom(d1), SearchExpr::DateFrom(d2)) => d1 == d2,
        (SearchExpr::DateTo(d1), SearchExpr::DateTo(d2)) => d1 == d2,
        (SearchExpr::And(se1, se2), SearchExpr::And(se3, se4)) => {
            test_eq(*se1, *se3) && test_eq(*se2, *se4)
        }
        (SearchExpr::Or(se1, se2), SearchExpr::Or(se3, se4)) => {
            test_eq(*se1, *se3) && test_eq(*se2, *se4)
        }
        (SearchExpr::Not(se1), SearchExpr::Not(se2)) => test_eq(*se1, *se2),
        _ => false,
    }
}
