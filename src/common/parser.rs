use nom::{
    branch::alt,
    bytes::complete::{escaped_transform, is_not, tag},
    character::streaming::none_of,
    combinator::{self, value},
    sequence::tuple,
    IResult,
};

/// parse an quoted string
fn parse_string_quoted(s: &str) -> IResult<&str, String> {
    alt((
        combinator::map(
            tuple((
                tag("\""),
                escaped_transform(
                    none_of("\"\\"),
                    '\\',
                    alt((value("\\", tag("\\")), value("\"", tag("\"")))),
                ),
                tag("\""),
            )),
            |x| x.1,
        ),
        value("".to_string(), tag("\"\"")),
    ))(s)
}

#[test]
fn test_parse_string_quoted() {
    assert_eq!(parse_string_quoted("\"\""), Ok(("", "".to_string())));
    assert_eq!(
        parse_string_quoted("\"a b c\\\" foo\""),
        Ok(("", "a b c\" foo".to_string()))
    );
    assert_eq!(
        parse_string_quoted("\"a b c\\\" foo\" bar"),
        Ok((" bar", "a b c\" foo".to_string()))
    );
}

/// parse an string with quotes or terminated by space
pub fn parse_text(s: &str) -> IResult<&str, String> {
    alt((
        parse_string_quoted,
        combinator::map(is_not(" ()"), String::from),
    ))(s)
}

#[test]
fn test_parse_text() {
    assert_eq!(parse_text("\"\""), Ok(("", "".to_string())));
    assert_eq!(
        parse_text("\"a b c\\\" foo\""),
        Ok(("", "a b c\" foo".to_string()))
    );
    assert_eq!(
        parse_text("\"a b c\\\" foo\" bar"),
        Ok((" bar", "a b c\" foo".to_string()))
    );
    assert_eq!(parse_text("foo"), Ok(("", "foo".to_string())));
    assert_eq!(parse_text("foo bar"), Ok((" bar", "foo".to_string())));
}
