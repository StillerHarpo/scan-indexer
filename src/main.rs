use clap::{Parser, Subcommand};
use std::env;
mod search_strings;
use search_strings::*;
mod common;
mod index_file;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// The root of directory where all the pdfs are stored
    scan_indexer_root: Option<String>,
    /// Command to run
    #[command(subcommand)]
    command: Command,
}

#[derive(Subcommand, Debug)]
enum Command {
    /// Search for a pdf
    Search {
        /// output as json
        #[arg(short, long)]
        json: bool,
        /// the search expression
        #[arg(value_parser = parse_search_string)]
        search_expr: SearchExpr,
    },
    /// Reindex according to the index file
    Reindex,
}

fn main() {
    let root_dir = env::var("SCAN_INDEXER_ROOT").unwrap_or("~/.scans".to_string());
    let args = Args::parse();
    println!("Root dir: {}", root_dir);
    println!("Args: {args:?}");
}
